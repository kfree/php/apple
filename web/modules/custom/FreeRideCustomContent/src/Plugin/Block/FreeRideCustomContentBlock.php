<?php

namespace Drupal\FreeRideCustomContent\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FreeRideCustomContent' Block.
 *
 * @Block(
 *   id = "FreeRideCustomContent_Block",
 *   admin_label = @Translation("FreeRideCustomContent Block"),
 *   category = @Translation("FreeRideCustomContent Block"),
 * )
 */
class FreeRideCustomContentBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build= array(
    	'#theme' => 'FreeRideCustomContent_Block',
    	 '#firstvar' => 'sravan',
    	  '#secondvar' => 24,
    	);
    return $build;
  }

}